// Write two ways of creating objects using functions that represent
// the items of the products on the products.jpg on the source directory

// Factory Function

function createFood(name, price) {
    return {
        name: name,
        price: price
    }
}

const food1 = createFood("Hot-dog", 10000);
const food2 = createFood("Lavash", 18000);
const food3 = createFood("Habmurger", 13000);
console.log(food1);
console.log(food2);
console.log(food3);

// Constructor Function

function CreateCar(name, price) {
    this.carName = name;
    this.carPrice = price;
}

const car1 = new CreateCar("Malibu 2", 25000);
const car2 = new CreateCar("Gentra", 13000);
const car3 = new CreateCar("Tracker", 30000);
console.log(car1);
console.log(car2);
console.log(car3);